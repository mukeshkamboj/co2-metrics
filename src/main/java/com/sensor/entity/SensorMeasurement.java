package com.sensor.entity;

import static javax.persistence.GenerationType.AUTO;
import static lombok.AccessLevel.PRIVATE;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "sensor_measurement")
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Builder(toBuilder = true)
@Getter
@Setter
public class SensorMeasurement {

  @Id
  @GeneratedValue(strategy = AUTO)
  private long id;

  @Column(nullable = false)
  private Integer co2Level;

  @Column(nullable = false)
  private Instant time;

  @ManyToOne
  private Sensor sensor;
}
