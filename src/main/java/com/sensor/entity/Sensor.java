package com.sensor.entity;

import static javax.persistence.GenerationType.AUTO;
import static lombok.AccessLevel.PRIVATE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "sensor", uniqueConstraints = {@UniqueConstraint(columnNames = {"uuid"})})
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE)
@Builder(toBuilder = true)
@Getter
@Setter
public class Sensor {

  @Id
  @GeneratedValue(strategy = AUTO)
  private long id;

  @Column(nullable = false)
  private String uuid;

  @Column(nullable = false)
  private String status;

  @Column(name = "alert_status_measure_counter", columnDefinition = "int default 0", nullable = false)
  private int alertStatusMeasureCounter;
}
