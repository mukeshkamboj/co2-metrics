package com.sensor.entity;

public enum SensorStatus {
  OK, WARN, ALERT
}
