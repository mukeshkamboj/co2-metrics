package com.sensor.service;

import static com.sensor.entity.SensorStatus.ALERT;
import static com.sensor.entity.SensorStatus.OK;
import static com.sensor.entity.SensorStatus.WARN;
import static com.sensor.exception.ErrorCode.SENSOR_ALREADY_EXISTS;
import static com.sensor.exception.ErrorCode.SENSOR_NOT_FOUND;
import static com.sensor.utils.DateUtil.getInstantDateOfNDaysBackFromNow;

import com.sensor.entity.Sensor;
import com.sensor.entity.SensorMeasurement;
import com.sensor.entity.SensorStatus;
import com.sensor.exception.ValidationException;
import com.sensor.repo.SensorMeasurementRepository;
import com.sensor.repo.SensorRepository;
import com.sensor.repo.dto.SensorMetrics;
import java.time.Instant;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SensorService {

  private final SensorRepository sensorRepository;
  private final SensorMeasurementRepository sensorMeasurementRepository;
  private static final int WARNING_CO2_LEVEL = 2000;
  private static final int ALERT_LEVEL_COUNTER_LIMIT = 3;
  private static final int METRIC_FOR_DAYS = 30;

  public String getSensorStatus(String uuid) {
    log.info("getSensorStatus is invoked with uuid : {}", uuid);
    return getSensor(uuid).getStatus();
  }

  @Transactional
  public long createSensor(String uuid) {
    log.info("SensorService.createSensor is invoked with uuid : {}", uuid);
    Optional<Sensor> sensorOptional = sensorRepository.findByUuid(uuid);
    sensorOptional.ifPresent((sensor) -> {
      throw new ValidationException("Sensor already exists with uuid : " + uuid,
          SENSOR_ALREADY_EXISTS);
    });

    Sensor sensor = Sensor.builder()
        .uuid(uuid)
        .status(OK.name())
        .build();

    return sensorRepository.save(sensor).getId();
  }

  @Transactional
  public long persistSensorMeasurement(String uuid, Integer co2Level, Instant time) {
    log.info(
        "SensorService.persistSensorMeasurement is invoked with uuid : {} co2Level : {} and time : {} ",
        uuid,
        co2Level,
        time
    );

    Sensor sensor = updateSensorStatusAndAlertStatusMeasureCounter(uuid, co2Level);
    return createSensorMeasurement(sensor, co2Level, time);
  }

  private Sensor updateSensorStatusAndAlertStatusMeasureCounter(String uuid, Integer co2Level) {
    Sensor sensor = getSensor(uuid);

    log.info(
        "Current details of sensor : {} are status : {} and alertStatusMeasureCounter : {}",
        uuid,
        sensor.getStatus(),
        sensor.getAlertStatusMeasureCounter()
    );

    updateSensorStatus(co2Level, sensor);

    log.info(
        "Updated the sensor status:  {} and alertStatusMeasureCounter : {}",
        sensor.getStatus(),
        sensor.getAlertStatusMeasureCounter()
    );
    return sensor;
  }

  private long createSensorMeasurement(Sensor sensor, Integer co2Level, Instant time) {

    SensorMeasurement sensorMeasurement = SensorMeasurement.builder()
        .co2Level(co2Level)
        .time(time)
        .sensor(sensor)
        .build();

    return sensorMeasurementRepository.save(sensorMeasurement).getId();
  }

  public SensorMetrics getSensorMetrics(String uuid) {
    log.info("SensorService.getSensorMetrics is invoked with uuid : {}", uuid);
    getSensor(uuid); // To validate the sensor existence.
    return sensorMeasurementRepository.getSensorMetricsForMeasurementsAfterDate(
        uuid,
        getInstantDateOfNDaysBackFromNow(METRIC_FOR_DAYS)
    );
  }

  private Sensor getSensor(String uuid) {
    return sensorRepository.findByUuid(uuid)
        .orElseThrow(
            () -> new ValidationException("Sensor not found with uuid : " + uuid, SENSOR_NOT_FOUND)
        );
  }

  private void updateSensorStatus(Integer co2Level, Sensor sensor) {
    SensorStatus sensorStatus;

    int alertStatusMeasureCounter = sensor.getAlertStatusMeasureCounter();

    if (isCo2LevelReachedWarningLevel(co2Level)
        && alertStatusMeasureCounter < ALERT_LEVEL_COUNTER_LIMIT) {
      alertStatusMeasureCounter++;
    } else if (isCo2LevelReachedOkLevel(co2Level) && alertStatusMeasureCounter > 0) {
      alertStatusMeasureCounter--;
    }

    if (isAlertStatusMeasureCounterLevelReachedAlertLevel(alertStatusMeasureCounter)) {
      sensorStatus = ALERT;
    } else if (isAlertStatusMeasureCounterLevelReachedOkLevel(alertStatusMeasureCounter)) {
      sensorStatus = OK;
    } else {
      sensorStatus = WARN;
    }

    sensor.setStatus(sensorStatus.name());
    sensor.setAlertStatusMeasureCounter(alertStatusMeasureCounter);
    sensorRepository.save(sensor);
  }

  private boolean isCo2LevelReachedWarningLevel(int co2Level) {
    return co2Level >= WARNING_CO2_LEVEL;
  }

  private boolean isCo2LevelReachedOkLevel(int co2Level) {
    return co2Level < WARNING_CO2_LEVEL;
  }

  private boolean isAlertStatusMeasureCounterLevelReachedAlertLevel(
      int currentAlertStatusMeasureCounter) {
    return currentAlertStatusMeasureCounter == ALERT_LEVEL_COUNTER_LIMIT;
  }

  private boolean isAlertStatusMeasureCounterLevelReachedOkLevel(
      int currentAlertStatusMeasureCounter) {
    return currentAlertStatusMeasureCounter == 0;
  }
}
