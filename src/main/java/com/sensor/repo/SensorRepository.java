package com.sensor.repo;

import com.sensor.entity.Sensor;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface SensorRepository extends CrudRepository<Sensor, Long> {

  Optional<Sensor> findByUuid(String uuid);
}
