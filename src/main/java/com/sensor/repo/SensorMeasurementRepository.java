package com.sensor.repo;

import com.sensor.entity.SensorMeasurement;
import com.sensor.repo.dto.SensorMetrics;
import java.time.Instant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SensorMeasurementRepository extends CrudRepository<SensorMeasurement, Long> {

  @Query(
      "SELECT "
          + " MAX(sm.co2Level) as maxLast30Days, AVG(sm.co2Level) as avgLast30Days "
          + "FROM "
          + " SensorMeasurement sm "
          + "WHERE "
          + " sm.sensor.uuid =:uuid "
          + " AND sm.time >=:endDate"
  )
  SensorMetrics getSensorMetricsForMeasurementsAfterDate(@Param("uuid") String uuid,
      @Param("endDate") Instant endDate);
}