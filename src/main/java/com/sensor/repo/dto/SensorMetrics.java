package com.sensor.repo.dto;

public interface SensorMetrics {
  Integer getMaxLast30Days();

  Integer getAvgLast30Days();
}
