package com.sensor.controller;

import com.sensor.dto.SensorMeasurementRequest;
import com.sensor.dto.SensorMetricsResponse;
import com.sensor.dto.SensorStatusResponse;
import com.sensor.repo.dto.SensorMetrics;
import com.sensor.service.SensorService;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/sensors")
@RequiredArgsConstructor
@Slf4j
public class SensorController {

  private final SensorService sensorService;

  @ApiOperation(value = "View sensor's status", response = SensorStatusResponse.class)
  @GetMapping("/{uuid}")
  public SensorStatusResponse getSensor(@PathVariable("uuid") String uuid) {
    log.info("getSensor is invoked with uuid : {}", uuid);
    return SensorStatusResponse.builder()
        .status(sensorService.getSensorStatus(uuid))
        .build();
  }

  @ApiOperation(value = "Create a sensor with uuid", response = Long.class)
  @PostMapping("/{uuid}")
  public Long createSensor(@PathVariable("uuid") String uuid) {
    log.info("createSensor is invoked with uuid : {}", uuid);
    return sensorService.createSensor(uuid);
  }

  @ApiOperation(value = "Create a sensor measurement", response = Long.class)
  @PostMapping("/{uuid}/measurements")
  public Long createSensorMeasurement(
      @PathVariable("uuid") String uuid,
      @Valid @RequestBody SensorMeasurementRequest measurementRequest
  ) {
    log.info("createSensorMeasurement is invoked with uuid : {} and measurement : {} ", uuid,
        measurementRequest);
    return sensorService.persistSensorMeasurement(uuid, measurementRequest.getCo2Level(),
        measurementRequest.getTime());
  }

  @ApiOperation(value = "Fetch the sensor's metrics", response = SensorMetricsResponse.class)
  @GetMapping("/{uuid}/metrics")
  public SensorMetricsResponse getSensorMetrics(@PathVariable("uuid") String uuid) {
    log.info("getSensorMetrics is invoked with uuid : {} ", uuid);
    SensorMetrics sensorMetrics = sensorService.getSensorMetrics(uuid);
    return SensorMetricsResponse.builder()
        .avgLast30Days(sensorMetrics.getAvgLast30Days())
        .maxLast30Days(sensorMetrics.getMaxLast30Days())
        .build();
  }
}
