package com.sensor.dto;

import static lombok.AccessLevel.PRIVATE;

import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@Data
@ToString
@Builder
public class SensorMeasurementRequest {

  @NotNull
  private final Integer co2Level;

  @NotNull
  private final Instant time;
}