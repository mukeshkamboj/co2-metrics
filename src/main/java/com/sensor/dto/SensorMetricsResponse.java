package com.sensor.dto;

import static lombok.AccessLevel.PRIVATE;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@Data
@ToString
@Builder
public class SensorMetricsResponse {

  private final Integer maxLast30Days;
  private final Integer avgLast30Days;
}
