package com.sensor.exception;

public enum ErrorCode {

  SENSOR_NOT_FOUND("SENSOR-4001"),
  SENSOR_ALREADY_EXISTS("SENSOR-4002");

  private final String code;

  ErrorCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}
