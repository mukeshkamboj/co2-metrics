package com.sensor.exception.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.sensor.dto.ErrorResponse;
import com.sensor.exception.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RestApiExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponse> genericExceptionHandler(Exception e) {
    log.error("Error occurred : ", e);
    ErrorResponse errorResponse = ErrorResponse.builder().build();
    return new ResponseEntity<>(errorResponse, INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ValidationException.class)
  public ResponseEntity<ErrorResponse> notFoundExceptionHandler(ValidationException e) {
    log.info(e.getMessage());
    ErrorResponse errorResponse = ErrorResponse.builder()
        .message(e.getMessage())
        .code(e.getErrorCode().getCode())
        .build();
    return new ResponseEntity<>(errorResponse, BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorResponse> methodArgumentNotValidExceptionHandler(
      MethodArgumentNotValidException e) {
    log.info(e.getMessage());
    ErrorResponse errorResponse = ErrorResponse.builder()
        .message("Required Parameter is missing.")
        .build();
    return new ResponseEntity<>(errorResponse, BAD_REQUEST);
  }
}
