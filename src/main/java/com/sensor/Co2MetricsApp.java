package com.sensor;

import static org.springframework.boot.SpringApplication.run;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Co2MetricsApp {

  public static void main(String[] args) {
    run(Co2MetricsApp.class);
  }
}
