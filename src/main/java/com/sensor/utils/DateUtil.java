package com.sensor.utils;

import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_DAY;
import static java.time.temporal.ChronoField.SECOND_OF_DAY;
import static java.time.temporal.ChronoUnit.DAYS;

import java.time.Instant;
import java.time.ZonedDateTime;

public class DateUtil {

  public static Instant getInstantDateOfNDaysBackFromNow(int days) {
    return ZonedDateTime.now()
        .withZoneSameInstant(UTC)
        .with(HOUR_OF_DAY, 0)
        .with(MINUTE_OF_DAY, 0)
        .with(SECOND_OF_DAY, 0)
        .toInstant()
        .minus(days, DAYS);
  }
}
