package com.sensor.utils;

import static com.sensor.utils.DateUtil.getInstantDateOfNDaysBackFromNow;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_DAY;
import static java.time.temporal.ChronoField.SECOND_OF_DAY;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.time.Instant;
import java.time.ZonedDateTime;
import org.junit.Test;

public class DateUtilTest {

  @Test
  public void GIVEN_days_WHEN_getInstantDateOfNDaysBackFromNow_is_invoked_THEN_date_minus_provided_days_is_returned() {
    //GIVEN
    int days = 30;
    Instant time = ZonedDateTime.now()
        .withZoneSameInstant(UTC)
        .with(HOUR_OF_DAY, 0)
        .with(MINUTE_OF_DAY, 0)
        .with(SECOND_OF_DAY, 0)
        .toInstant()
        .minus(days, DAYS);

    //WHEN
    Instant response = getInstantDateOfNDaysBackFromNow(days);

    //THEN
    assertThat(response.truncatedTo(SECONDS), is(time.truncatedTo(SECONDS)));
  }
}