package com.sensor.controller;

import static com.sensor.SensorTestUtil.createSensorMeasurementRequest;
import static com.sensor.entity.SensorStatus.ALERT;
import static com.sensor.entity.SensorStatus.OK;
import static com.sensor.entity.SensorStatus.WARN;
import static com.sensor.exception.ErrorCode.SENSOR_ALREADY_EXISTS;
import static com.sensor.exception.ErrorCode.SENSOR_NOT_FOUND;
import static com.sensor.utils.DateUtil.getInstantDateOfNDaysBackFromNow;
import static java.lang.Long.parseLong;
import static java.time.Instant.EPOCH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sensor.SpringBootBaseTest;
import com.sensor.dto.ErrorResponse;
import com.sensor.dto.SensorMeasurementRequest;
import com.sensor.dto.SensorMetricsResponse;
import com.sensor.dto.SensorStatusResponse;
import com.sensor.entity.Sensor;
import com.sensor.entity.SensorMeasurement;
import com.sensor.entity.SensorStatus;
import com.sensor.repo.SensorMeasurementRepository;
import com.sensor.repo.SensorRepository;
import java.time.Instant;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SensorControllerTest extends SpringBootBaseTest {

  private final static String SENSOR_URI_PREFIX = "/v1/sensors/";
  private final static String SENSOR_MEASUREMENT_URI = SENSOR_URI_PREFIX + "%s" + "/measurements";
  private final static String SENSOR_METRICS_URI = SENSOR_URI_PREFIX + "%s" + "/metrics";

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private SensorMeasurementRepository sensorMeasurementRepository;

  @Autowired
  private SensorRepository sensorRepository;

  private final int CO2_WARNING_LEVEL = 2000;
  private final int CO2_OKAY_LEVEL = 100;

  @Test
  @SneakyThrows
  public void GIVEN_sensor_WHEN_get_sensor_status_endpoint_is_invoked_THEN_sensor_status_is_returned() {
    //GIVEN
    String sensorUuid = createSensor();

    //WHEN
    ResultActions response = mvc
        .perform(get(SENSOR_URI_PREFIX + sensorUuid).accept(APPLICATION_JSON));

    //THEN
    response.andExpect(status().isOk());
    response.andExpect(content().contentType(APPLICATION_JSON_UTF8));
    assertThat(sensorMeasurementRepository.count(), is(0L));
    assertThat(sensorRepository.count(), is(1L));
    response.andExpect(mvcResult -> {
      SensorStatusResponse sensorStatusResponse = objectMapper.readValue(
          mvcResult.getResponse().getContentAsString(),
          SensorStatusResponse.class
      );
      assertThat(sensorStatusResponse.getStatus(), is(OK.name()));
    });
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_WHEN_get_sensor_status_endpoint_is_invoked_with_invalid_uuid_THEN_api_returns_4001() {
    //GIVEN
    String notExistingSensorUuid = "sensor-2";

    //WHEN
    ResultActions response = mvc
        .perform(get(SENSOR_URI_PREFIX + notExistingSensorUuid).accept(APPLICATION_JSON));

    //THEN
    validateSensorNotExist(notExistingSensorUuid, response);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_WHEN_post_sensor_endpoint_is_called_THEN_sensor_is_created_in_system() {
    //GIVEN
    String sensorUuid = "sensor-1";

    //WHEN
    ResultActions response = mvc
        .perform(post(SENSOR_URI_PREFIX + sensorUuid).accept(APPLICATION_JSON));

    //THEN
    response.andExpect(status().isOk());
    response.andExpect(content().contentType(APPLICATION_JSON_UTF8));
    assertThat(sensorRepository.count(), is(1L));
    assertThat(sensorMeasurementRepository.count(), is(0L));
    response.andExpect(mvcResult -> {
      assertThat(getIdFromResponse(mvcResult), is(greaterThan(0L)));
      Optional<Sensor> sensorOptional = sensorRepository.findById(getIdFromResponse(mvcResult));
      assertThat(sensorOptional.isPresent(), is(true));
      assertThat(sensorOptional.get().getUuid(), is(sensorUuid));
      assertThat(sensorOptional.get().getStatus(), is(OK.name()));
    });
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_measurement_with_okay_co2_level_WHEN_post_measurement_endpoint_is_called_THEN_measurement_is_created_in_system_with_okay_level() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest request = createSensorMeasurementRequest(CO2_OKAY_LEVEL, EPOCH);

    //WHEN
    ResultActions response = postSensorMeasurement(sensorUuid, request);

    //THEN
    validateSensorMeasurement(sensorUuid, request, response, OK, 0, 1L);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_measurement_with_warning_co2_level_WHEN_post_measurement_endpoint_is_called_THEN_measurement_is_created_in_system_with_warn_level() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest request = createSensorMeasurementRequest(CO2_WARNING_LEVEL, EPOCH);

    //WHEN
    ResultActions response = postSensorMeasurement(sensorUuid, request);

    //THEN
    validateSensorMeasurement(sensorUuid, request, response, WARN, 1, 1L);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_measurement_with_warning_co2_level_3_time_WHEN_post_measurement_endpoint_is_called_THEN_measurement_is_created_in_system_with_alert_level() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest request = createSensorMeasurementRequest(CO2_WARNING_LEVEL, EPOCH);

    //WHEN
    postSensorMeasurement(sensorUuid, request);
    postSensorMeasurement(sensorUuid, request);
    ResultActions response = postSensorMeasurement(sensorUuid, request);

    //THEN
    validateSensorMeasurement(sensorUuid, request, response, ALERT, 3, 3L);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_with_warning_level_alert_WHEN_post_measurement_endpoint_is_called_with_okay_level_3_times_consecutive_THEN_sensor_status_changed_to_okay_level() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest requestWithWarningCo2Level = createSensorMeasurementRequest(CO2_WARNING_LEVEL, EPOCH);
    SensorMeasurementRequest requestWithOkayCo2Level = createSensorMeasurementRequest(CO2_OKAY_LEVEL, EPOCH);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);

    //WHEN
    postSensorMeasurement(sensorUuid, requestWithOkayCo2Level);
    postSensorMeasurement(sensorUuid, requestWithOkayCo2Level);
    ResultActions response = postSensorMeasurement(sensorUuid, requestWithOkayCo2Level);

    //THEN
    validateSensorMeasurement(sensorUuid, requestWithOkayCo2Level, response, OK, 0, 6L);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_with_status_alert_WHEN_post_measurement_endpoint_is_called_with_okay_level_2_times_consecutive_THEN_sensor_status_changed_to_warning_level() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest requestWithWarningCo2Level = createSensorMeasurementRequest(CO2_WARNING_LEVEL, EPOCH);
    SensorMeasurementRequest requestWithOkayCo2Level = createSensorMeasurementRequest(CO2_OKAY_LEVEL, EPOCH);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);
    postSensorMeasurement(sensorUuid, requestWithWarningCo2Level);

    //WHEN
    postSensorMeasurement(sensorUuid, requestWithOkayCo2Level);
    ResultActions response = postSensorMeasurement(sensorUuid, requestWithOkayCo2Level);

    //THEN
    validateSensorMeasurement(sensorUuid, requestWithOkayCo2Level, response, WARN, 1, 5L);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_measurements_WHEN_sensor_metrics_endpoint_is_called_THEN_sensor_metrics_is_returned() {
    //GIVEN
    String sensorUuid = createSensor();
    SensorMeasurementRequest request = createSensorMeasurementRequest(CO2_WARNING_LEVEL,
        Instant.now());
    postSensorMeasurement(sensorUuid, request);

    request = createSensorMeasurementRequest(CO2_OKAY_LEVEL, Instant.now());
    postSensorMeasurement(sensorUuid, request);

    request = createSensorMeasurementRequest(CO2_OKAY_LEVEL, getInstantDateOfNDaysBackFromNow(31));
    postSensorMeasurement(sensorUuid, request);

    //WHEN
    ResultActions response = mvc
        .perform(get(getSensorMetricsUri(sensorUuid)).accept(APPLICATION_JSON));

    //THEN
    response.andExpect(status().isOk());
    response.andExpect(content().contentType(APPLICATION_JSON_UTF8));
    response.andExpect(mvcResult -> {
      SensorMetricsResponse sensorMetrics = objectMapper.readValue(
          mvcResult.getResponse().getContentAsString(),
          SensorMetricsResponse.class
      );
      assertThat(sensorMetrics.getAvgLast30Days(), is(1050));
      assertThat(sensorMetrics.getMaxLast30Days(), is(CO2_WARNING_LEVEL));
    });
  }

  @Test
  @SneakyThrows
  public void GIVEN_non_existing_sensor_uuid_WHEN_sensor_metrics_endpoint_is_called_with_wrong_uuid_THEN_4001_error_code_is_returned() {
    //GIVEN
    String nonExistingSensorUuid = "sensor-2";

    //WHEN
    ResultActions response = mvc
        .perform(get(getSensorMetricsUri(nonExistingSensorUuid)).accept(APPLICATION_JSON));

    //THEN
    response.andExpect(status().isBadRequest());
    validateSensorNotExist(nonExistingSensorUuid, response);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_measurement_and_senor_uuid_does_not_exist_WHEN_post_measurement_endpoint_is_called_THEN_api_returns_4001() {
    //GIVEN
    String notExistingSensorUuid = "sensor-2";
    SensorMeasurementRequest request = createSensorMeasurementRequest(CO2_OKAY_LEVEL, EPOCH);

    //WHEN
    ResultActions response = postSensorMeasurement(notExistingSensorUuid, request);

    //THEN
    validateSensorNotExist(notExistingSensorUuid, response);
  }

  @Test
  public void GIVEN_time_is_missing_WHEN_measurement_end_point_is_invoked_THEN_400_is_returned() {
    SensorMeasurementRequest request = SensorMeasurementRequest.builder()
        .co2Level(CO2_OKAY_LEVEL)
        .build();
    validateMissingFieldsInMeasurementRequest(request);
  }

  @Test
  public void GIVEN_co2Level_is_missing_WHEN_measurement_end_point_is_invoked_THEN_400_is_returned() {
    SensorMeasurementRequest request = SensorMeasurementRequest.builder()
        .time(EPOCH)
        .build();
    validateMissingFieldsInMeasurementRequest(request);
  }

  @Test
  @SneakyThrows
  public void GIVEN_sensor_uuid_already_exists_WHEN_create_sensor_end_point_is_invoked_THEN_4002_is_returned() {
    //GIVEN
    String sensorUuid = createSensor();

    //WHEN
    ResultActions response = mvc
        .perform(post(SENSOR_URI_PREFIX + sensorUuid).accept(APPLICATION_JSON));

    //THEN
    response.andExpect(status().isBadRequest());
    assertThat(sensorMeasurementRepository.count(), is(0L));
    assertThat(sensorRepository.count(), is(1L));
    response.andExpect(mvcResult -> {
          ErrorResponse errorResponse = getErrorResponseFromResponse(mvcResult);
          assertThat(errorResponse.getCode(), is(SENSOR_ALREADY_EXISTS.getCode()));
          assertThat(errorResponse.getMessage(), is("Sensor already exists with uuid : " + sensorUuid));
        }
    );
  }

  @SneakyThrows
  private void validateSensorMeasurement(
      String sensorUuid,
      SensorMeasurementRequest request,
      ResultActions response,
      SensorStatus sensorStatus,
      int alertStatusMeasureCounter,
      long countOfSensorMeasurement) {
    response.andExpect(status().isOk());
    response.andExpect(content().contentType(APPLICATION_JSON_UTF8));
    assertThat(sensorMeasurementRepository.count(), is(countOfSensorMeasurement));
    assertThat(sensorRepository.count(), is(1L));
    response.andExpect(mvcResult -> {
          assertThat(getIdFromResponse(mvcResult), is(greaterThan(0L)));
          Optional<SensorMeasurement> sensorOptional = sensorMeasurementRepository
              .findById(getIdFromResponse(mvcResult));
          assertThat(sensorOptional.isPresent(), is(true));
          SensorMeasurement sensorMeasurement = sensorOptional.get();
          assertThat(sensorMeasurement.getCo2Level(), is(request.getCo2Level()));
          assertThat(sensorMeasurement.getTime(), is(request.getTime()));
          assertThat(sensorMeasurement.getSensor(), is(notNullValue()));
          assertThat(sensorMeasurement.getSensor().getUuid(), is(sensorUuid));
          assertThat(sensorMeasurement.getSensor().getStatus(), is(sensorStatus.name()));
          assertThat(sensorMeasurement.getSensor().getAlertStatusMeasureCounter(),
              is(alertStatusMeasureCounter));
        }
    );
  }

  @SneakyThrows
  private void validateMissingFieldsInMeasurementRequest(SensorMeasurementRequest request) {
    //GIVEN
    String sensorUuid = createSensor();

    //WHEN
    ResultActions response = postSensorMeasurement(sensorUuid, request);

    //THEN
    response.andExpect(status().isBadRequest());
    assertThat(sensorMeasurementRepository.count(), is(0L));
    assertThat(sensorRepository.count(), is(1L));
    response.andExpect(mvcResult -> {
          ErrorResponse errorResponse = getErrorResponseFromResponse(mvcResult);
          assertThat(errorResponse.getMessage(), is("Required Parameter is missing."));
        }
    );
  }

  @SneakyThrows
  private ResultActions postSensorMeasurement(String sensorUuid, SensorMeasurementRequest request) {
    return mvc.perform(
        post(getSensorMeasurementUri(sensorUuid))
            .content(objectMapper.writeValueAsString(request))
            .contentType(APPLICATION_JSON_UTF8)
            .accept(APPLICATION_JSON_UTF8));
  }

  private String getSensorMeasurementUri(String sensorUuid) {
    return String.format(SENSOR_MEASUREMENT_URI, sensorUuid);
  }

  private String getSensorMetricsUri(String sensorUuid) {
    return String.format(SENSOR_METRICS_URI, sensorUuid);
  }

  @SneakyThrows
  private ErrorResponse getErrorResponseFromResponse(MvcResult mvcResult) {
    return objectMapper
        .readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
  }

  @SneakyThrows
  private Long getIdFromResponse(MvcResult mvcResult) {
    return parseLong(mvcResult.getResponse().getContentAsString());
  }

  private void validateSensorNotExist(String notExistingSensorUuid, ResultActions response)
      throws Exception {
    response.andExpect(status().isBadRequest());
    response.andExpect(content().contentType(APPLICATION_JSON_UTF8));
    assertThat(sensorMeasurementRepository.count(), is(0L));
    assertThat(sensorRepository.count(), is(0L));
    response.andExpect(mvcResult -> {
      ErrorResponse errorResponse = getErrorResponseFromResponse(mvcResult);
      assertThat(errorResponse.getCode(), is(SENSOR_NOT_FOUND.getCode()));
      assertThat(errorResponse.getMessage(),
          is("Sensor not found with uuid : " + notExistingSensorUuid));
    });
  }

  private String createSensor() {
    String sensorUuid = "sensor-10";
    Sensor sensor = Sensor.builder()
        .uuid(sensorUuid)
        .status(OK.name())
        .build();
    sensorRepository.save(sensor);
    return sensorUuid;
  }
}
