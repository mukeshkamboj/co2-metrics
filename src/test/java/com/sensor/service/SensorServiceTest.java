package com.sensor.service;

import static com.sensor.SensorTestUtil.createSensor;
import static com.sensor.SensorTestUtil.createSensorMeasurement;
import static com.sensor.entity.SensorStatus.ALERT;
import static com.sensor.entity.SensorStatus.OK;
import static com.sensor.entity.SensorStatus.WARN;
import static java.time.Instant.EPOCH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.sensor.entity.Sensor;
import com.sensor.entity.SensorMeasurement;
import com.sensor.entity.SensorStatus;
import com.sensor.exception.ValidationException;
import com.sensor.repo.SensorMeasurementRepository;
import com.sensor.repo.SensorRepository;
import com.sensor.repo.dto.SensorMetrics;
import java.time.Instant;
import java.util.Optional;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;

public class SensorServiceTest {

  private static final int CO2_WARN_LEVEL = 2000;
  private static final int CO2_OK_LEVEL = 100;
  private SensorService sensorService;

  private SensorRepository sensorRepository = mock(SensorRepository.class);

  private SensorMeasurementRepository sensorMeasurementRepository = mock(
      SensorMeasurementRepository.class);

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setup() {
    sensorService = new SensorService(sensorRepository, sensorMeasurementRepository);
  }

  @Test
  public void GIVEN_sensor_already_exist_with_uuid_WHEN_create_sensor_method_is_invoked_THEN_validation_exception_is_thrown() {
    //GIVEN
    String sensorUuid = "sensor-1";
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(Sensor.builder().build()));

    //THEN
    expectedException.expect(ValidationException.class);

    //WHEN
    sensorService.createSensor(sensorUuid);
  }

  @Test
  public void GIVEN_sensor_with_uuid_WHEN_create_sensor_method_is_invoked_THEN_sensor_repository_save_function_is_invoked() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.empty());
    when(sensorRepository.save(any())).thenReturn(Sensor.builder().id(sensorId).build());

    //WHEN
    long response = sensorService.createSensor(sensorUuid);

    //THEN
    ArgumentCaptor<Sensor> sensorCaptor = ArgumentCaptor.forClass(Sensor.class);
    verify(sensorRepository, times(1)).save(sensorCaptor.capture());
    assertThat(response, is(sensorId));
    assertThat(sensorCaptor.getAllValues().get(0).getUuid(), is(sensorUuid));
    assertThat(sensorCaptor.getAllValues().get(0).getStatus(), is(OK.name()));
    assertThat(sensorCaptor.getAllValues().get(0).getAlertStatusMeasureCounter(), is(0));
  }

  @Test
  public void GIVEN_sensor_measurement_with_warn_level_and_sensor_with_alertStatusMeasureCounter_zero_WHEN_create_sensor_measurement_method_is_invoked_THEN_sensor_status_changed_to_warn_and_alertStatusMeasureCounter_to_1() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    long sensorMeasurementId = 1L;
    int co2Level = CO2_WARN_LEVEL;
    Instant time = Instant.now();
    Sensor sensor = createSensor(sensorId, sensorUuid, 0, OK);
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(sensor));
    SensorMeasurement sensorMeasurement = createSensorMeasurement(sensor, sensorMeasurementId, 50,
        EPOCH);
    when(sensorMeasurementRepository.save(any())).thenReturn(sensorMeasurement);

    //WHEN
    long response = sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);

    //THEN
    validateSenorMeasurement(1, sensorId, sensorMeasurementId, co2Level, time, response, WARN, 1);
  }

  @Test
  public void GIVEN_sensor_measurement_with_warn_level_and_sensor_with_alertStatusMeasureCounter_2_WHEN_create_sensor_measurement_method_is_invoked_THEN_sensor_status_changed_to_ALERT_and_alertStatusMeasureCounter_to_3() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    long sensorMeasurementId = 1L;
    int co2Level = CO2_WARN_LEVEL;
    Instant time = Instant.now();
    Sensor sensor = createSensor(sensorId, sensorUuid, 2, WARN);
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(sensor));
    SensorMeasurement sensorMeasurement = createSensorMeasurement(sensor, sensorMeasurementId, 50,
        EPOCH);
    when(sensorMeasurementRepository.save(any())).thenReturn(sensorMeasurement);

    //WHEN
    long response = sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);

    //THEN
    validateSenorMeasurement(1, sensorId, sensorMeasurementId, co2Level, time, response, ALERT, 3);
  }

  @Test
  public void GIVEN_sensor_measurement_with_ok_level_and_sensor_with_alertStatusMeasureCounter_3_WHEN_create_sensor_measurement_method_is_invoked_THEN_sensor_status_changed_to_WARN_and_alertStatusMeasureCounter_to_2() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    long sensorMeasurementId = 1L;
    int co2Level = CO2_OK_LEVEL;
    Instant time = Instant.now();
    Sensor sensor = createSensor(sensorId, sensorUuid, 3, ALERT);
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(sensor));
    SensorMeasurement sensorMeasurement = createSensorMeasurement(sensor, sensorMeasurementId, 50,
        EPOCH);
    when(sensorMeasurementRepository.save(any())).thenReturn(sensorMeasurement);

    //WHEN
    long response = sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);

    //THEN
    validateSenorMeasurement(1, sensorId, sensorMeasurementId, co2Level, time, response, WARN, 2);
  }

  @Test
  public void GIVEN_sensor_measurement_with_ok_level_and_sensor_with_alertStatusMeasureCounter_3_WHEN_create_sensor_measurement_method_is_invoked_more_than_3_times_THEN_sensor_status_changed_to_OK_and_alertStatusMeasureCounter_to_0() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    long sensorMeasurementId = 1L;
    int co2Level = CO2_OK_LEVEL;
    Instant time = Instant.now();
    Sensor sensor = createSensor(sensorId, sensorUuid, 3, ALERT);
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(sensor));
    SensorMeasurement sensorMeasurement = createSensorMeasurement(sensor, sensorMeasurementId, 50,
        EPOCH);
    when(sensorMeasurementRepository.save(any())).thenReturn(sensorMeasurement);

    //WHEN
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    long response = sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);

    //THEN
    validateSenorMeasurement(4, sensorId, sensorMeasurementId, co2Level, time, response, OK, 0);
  }

  @Test
  public void GIVEN_sensor_measurement_with_WARN_level_and_sensor_with_alertStatusMeasureCounter_0_WHEN_create_sensor_measurement_method_is_invoked_more_than_3_times_THEN_sensor_status_changed_to_ALERT_and_alertStatusMeasureCounter_to_3() {
    //GIVEN
    String sensorUuid = "sensor-1";
    long sensorId = 1L;
    long sensorMeasurementId = 1L;
    int co2Level = CO2_WARN_LEVEL;
    Instant time = Instant.now();
    Sensor sensor = createSensor(sensorId, sensorUuid, 0, OK);
    when(sensorRepository.findByUuid(sensorUuid)).thenReturn(Optional.of(sensor));
    SensorMeasurement sensorMeasurement = createSensorMeasurement(sensor, sensorMeasurementId, 50,
        EPOCH);
    when(sensorMeasurementRepository.save(any())).thenReturn(sensorMeasurement);

    //WHEN
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);
    long response = sensorService.persistSensorMeasurement(sensorUuid, co2Level, time);

    //THEN
    validateSenorMeasurement(4, sensorId, sensorMeasurementId, co2Level, time, response, ALERT, 3);
  }

  @Test
  public void GIVEN_sensor_measurements_WHEN_getSensorMetrics_is_invoked_THEN_sensor_metrics_are_returned() {
    //GIVEN
    SensorMetrics sensorMetrics = mock(SensorMetrics.class);
    when(sensorMetrics.getAvgLast30Days()).thenReturn(300);
    when(sensorMetrics.getMaxLast30Days()).thenReturn(300);
    String uuid = "sensor-1";
    when(sensorRepository.findByUuid(uuid)).thenReturn(Optional.of(Sensor.builder().build()));
    when(sensorMeasurementRepository.getSensorMetricsForMeasurementsAfterDate(any(), any()))
        .thenReturn(sensorMetrics);

    //WHEN
    SensorMetrics response = sensorService.getSensorMetrics(uuid);

    //THEN
    assertThat(response.getMaxLast30Days(), is(300));
    assertThat(response.getAvgLast30Days(), is(300));
  }

  @Test
  public void GIVEN_non_existing_sensor_uuid_WHEN_getSensorMetrics_is_invoked_THEN_validation_exception_is_thrown() {
    //GIVEN
    String uuid = "sensor-1";
    when(sensorRepository.findByUuid(uuid)).thenReturn(Optional.empty());

    //THEN
    expectedException.expect(ValidationException.class);

    //WHEN
    sensorService.getSensorMetrics(uuid);
  }

  @Test
  public void GIVEN_non_existing_sensor_uuid_WHEN_create_sensor_measurement_is_invoked_THEN_validation_exception_is_thrown() {
    //GIVEN
    String uuid = "sensor-1";
    when(sensorRepository.findByUuid(uuid)).thenReturn(Optional.empty());

    //THEN
    expectedException.expect(ValidationException.class);

    //WHEN
    sensorService.persistSensorMeasurement(uuid, 200, EPOCH);
  }

  private void validateSenorMeasurement(int saveMethodCalledTimes, long sensorId,
      long sensorMeasurementId, int co2Level, Instant time, long response,
      SensorStatus sensorStatus, int i) {
    ArgumentCaptor<SensorMeasurement> sensorCaptor = ArgumentCaptor
        .forClass(SensorMeasurement.class);
    verify(sensorMeasurementRepository, times(saveMethodCalledTimes)).save(sensorCaptor.capture());
    verify(sensorRepository, times(saveMethodCalledTimes)).save(any());
    assertThat(response, is(sensorMeasurementId));
    SensorMeasurement actual = sensorCaptor.getAllValues().get(0);
    assertThat(response, is(sensorMeasurementId));
    assertThat(actual.getTime(), is(time));
    assertThat(actual.getCo2Level(), is(co2Level));
    assertThat(actual.getSensor(), is(notNullValue()));
    assertThat(actual.getSensor().getId(), is(sensorId));
    assertThat(actual.getSensor().getStatus(), is(sensorStatus.name()));
    assertThat(actual.getSensor().getAlertStatusMeasureCounter(), is(i));
  }
}