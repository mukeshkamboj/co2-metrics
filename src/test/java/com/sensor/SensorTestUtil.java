package com.sensor;

import com.sensor.dto.SensorMeasurementRequest;
import com.sensor.entity.Sensor;
import com.sensor.entity.SensorMeasurement;
import com.sensor.entity.SensorStatus;
import java.time.Instant;

public class SensorTestUtil {

  public static SensorMeasurementRequest createSensorMeasurementRequest(int co2Level,
      Instant time) {
    return SensorMeasurementRequest.builder()
        .co2Level(co2Level)
        .time(time)
        .build();
  }

  public static SensorMeasurement createSensorMeasurement(Sensor sensor, int co2Level,
      Instant time) {
    return SensorMeasurement.builder()
        .sensor(sensor)
        .co2Level(co2Level)
        .time(time)
        .build();
  }

  public static SensorMeasurement createSensorMeasurement(Sensor sensor, long sensorMeasurementId,
      int co2Level, Instant time) {
    return SensorMeasurement.builder()
        .id(sensorMeasurementId)
        .sensor(sensor)
        .co2Level(co2Level)
        .time(time)
        .build();
  }

  public static Sensor createSensor(Long sensorId, String sensorUuid, int alertStatusMeasureCounter,
      SensorStatus status) {
    return Sensor.builder()
        .id(sensorId)
        .uuid(sensorUuid)
        .status(status.name())
        .alertStatusMeasureCounter(alertStatusMeasureCounter)
        .build();
  }
}
