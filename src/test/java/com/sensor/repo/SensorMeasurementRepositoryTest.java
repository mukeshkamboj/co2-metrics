package com.sensor.repo;

import static com.sensor.SensorTestUtil.createSensorMeasurement;
import static com.sensor.entity.SensorStatus.OK;
import static java.time.Instant.EPOCH;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.sensor.SpringBootBaseTest;
import com.sensor.entity.Sensor;
import com.sensor.entity.SensorMeasurement;
import com.sensor.repo.dto.SensorMetrics;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SensorMeasurementRepositoryTest extends SpringBootBaseTest {

  @Autowired
  private SensorMeasurementRepository sensorMeasurementRepository;

  @Autowired
  private SensorRepository sensorRepository;

  @Test
  public void GIVEN_sensor_measurements_WHEN_getSensorMetricsForMeasurementsAfterDate_is_invoked_THEN_metrics_includes_records_after_and_on_date_is_returned() {
    //GIVEN
    String sensorUuid = "sensor-1";
    Sensor sensor = sensorRepository
        .save(Sensor.builder().uuid(sensorUuid).status(OK.name()).build());
    List<SensorMeasurement> sensorMeasurements = Arrays.asList(
        createSensorMeasurement(sensor, 50, EPOCH),
        createSensorMeasurement(sensor, 50, EPOCH),
        createSensorMeasurement(sensor, 2000, EPOCH),
        createSensorMeasurement(sensor, 3000, EPOCH.minus(1, DAYS))
    );
    sensorMeasurementRepository.saveAll(sensorMeasurements);

    //WHEN
    SensorMetrics sensorMetrics = sensorMeasurementRepository
        .getSensorMetricsForMeasurementsAfterDate(sensorUuid, EPOCH);

    //THEN
    assertThat(sensorMetrics.getAvgLast30Days(), is(700));
    assertThat(sensorMetrics.getMaxLast30Days(), is(2000));
  }

  @Test
  public void GIVEN_sensor_measurements_for_many_sensors_WHEN_getSensorMetricsForMeasurementsAfterDate_is_invoked_THEN_metrics_includes_records_after_and_on_date_and_for_provided_sensor_uuid_is_returned() {
    //GIVEN
    String sensorUuid1 = "sensor-1";
    String sensorUuid2 = "sensor-2";
    Sensor sensor1 = sensorRepository
        .save(Sensor.builder().uuid(sensorUuid1).status(OK.name()).build());
    Sensor sensor2 = sensorRepository
        .save(Sensor.builder().uuid(sensorUuid2).status(OK.name()).build());
    List<SensorMeasurement> sensorMeasurements = Arrays.asList(
        createSensorMeasurement(sensor1, 50, EPOCH),
        createSensorMeasurement(sensor2, 50, EPOCH),
        createSensorMeasurement(sensor1, 2000, EPOCH),
        createSensorMeasurement(sensor2, 3000, EPOCH.minus(1, DAYS))
    );
    sensorMeasurementRepository.saveAll(sensorMeasurements);

    //WHEN
    SensorMetrics sensorMetrics = sensorMeasurementRepository
        .getSensorMetricsForMeasurementsAfterDate(sensorUuid1, EPOCH);

    //THEN
    assertThat(sensorMetrics.getAvgLast30Days(), is(1025));
    assertThat(sensorMetrics.getMaxLast30Days(), is(2000));
  }
}