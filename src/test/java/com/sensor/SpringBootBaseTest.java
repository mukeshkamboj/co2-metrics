package com.sensor;

import com.sensor.repo.SensorMeasurementRepository;
import com.sensor.repo.SensorRepository;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = Co2MetricsApp.class)
public class SpringBootBaseTest {

  @Autowired
  private SensorMeasurementRepository sensorMeasurementRepository;

  @Autowired
  private SensorRepository sensorRepository;

  @Before
  public void clearDataBase() {
    sensorMeasurementRepository.deleteAll();
    sensorRepository.deleteAll();
  }
}
