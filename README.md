#CO2 Level Sensor Measurements REST API.

## Tech Stack Used
- Java 8
- Spring Boot
- H2 Database In Memory
- Swagger for REST API documentation
- Lombok lib to avoid boilerplate code for getter, builder patters.
- Spring Boot Test, Mockito, hamcrest for unit testing.


## Execute the test
```
mvn clean test
```

## Build the application
```
mvn clean install
```

## Run the application
```
mvn clean install spring-boot:run
```

## Access the application
```
http://localhost:8080/api/swagger-ui.html#/
```
Swagger will present the REST API documentation. You can easily follow the documentation and access the rest APIs.  
APIs can be provided the input and perform actions through swagger API. It will describe the request & response format etc.

## Development Setup
- Please import the intellij-java-google-style.xml for code styles.
- Please install the lombok plugin. Please refer the website https://projectlombok.org/setup/overview